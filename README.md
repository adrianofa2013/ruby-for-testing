# Ruby For Testing

*Hello, this is a project that I made to learn Ruby.*

*To execute this script you is necessary to you have ruby on your machine:*

*Link instalation Ruby: https://www.ruby-lang.org/pt/*

*To clone this project on your machine use in git bash:*

`git clone https://gitlab.com/adrianofa2013/ruby-for-testing.git`

*From the main path, go to 'avancado' or 'basica'*

```
cd path\
Example: cd basico\
```

*Then, to execute the scripts use:*
```
ruby <script.rb>
Example: ruby hello.rb
```
